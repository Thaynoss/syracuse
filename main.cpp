#include <iostream>

using namespace std;

const int MAX_N = 2048;

int main() {
    int U[MAX_N];
    int max_n_effectif;

    cout << "Saisir l'entier pour la valeur U0 "; cin >> U[0];


    cout << "Saisir à quel n doit-on s'arrêter ?"; cin >> max_n_effectif;

    for (int i = 1; i <= max_n_effectif; i = i + 1) {
        if (U[i - 1] % 2 == 0) {
            U[i] = U[i - 1] / 2;
        } else {
            U[i] = 3 * U[i - 1] + 1;
        }
        cout << i << ": " << U[i] << endl;
    }

    return 0;
}

